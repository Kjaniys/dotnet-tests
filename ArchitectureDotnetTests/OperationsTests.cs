﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ArchitectureDotnet.Tests
{
    [TestClass()]
    public class OperationsTests
    {
        [TestMethod()]
        public void SumTest()
        {
            Assert.AreEqual(4, Operations.Sum(2, 2));
        }
    }
}