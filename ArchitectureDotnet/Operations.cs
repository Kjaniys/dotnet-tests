﻿
namespace ArchitectureDotnet
{
    /// <summary>
    /// Утилитарный статический класс, предоставляющий методы для выполнения арифметических
    /// операций
    /// </summary>
    public static class Operations
    {
        /// <summary>
        /// Метод, возвращающий сумму двух операндов
        /// </summary>
        /// <param name="a">Первый операнд</param>
        /// <param name="b">Второй операнд</param>
        /// <returns>Сумма операндов</returns>
        public static int Sum(int a, int b) => a + b;
    }
}
